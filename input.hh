#ifndef INPUT_HH
#define INPUT_HH

#include <string>
#include <istream>

// reads everything from input and stores it
// in the return value
std::string read_stream(std::istream& input);

#endif // INPUT_HH
